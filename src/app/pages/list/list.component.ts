import { Card } from './../../interfaces/card.interface';
import { CardService } from './../../services/card.service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  cards: Card[] = [];
  offset = 0;
  cardTextFC = new FormControl('');

  constructor(private cardService: CardService) { }

  ngOnInit(): void {
    this.cardTextFC.valueChanges.pipe(debounceTime(1000)).subscribe( res =>{
      console.log(res);
      this.cards = [];
      this.searchCards(res);
    });
    this.searchCards();
    // this.listCards();
  }

  onScroll(){
    console.log('scroll');
    this.offset += 100;
    this.searchCards();
  }

  searchCards(cardName: string | null = null) {
    this.cardService.getCards(cardName, this.offset).subscribe(res => {
      console.log(res);
      this.cards = [...this.cards,  ...res];
    })
  }

}
