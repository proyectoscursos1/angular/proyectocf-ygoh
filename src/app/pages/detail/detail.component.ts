import { CardService } from './../../services/card.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Card } from 'src/app/interfaces/card.interface';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private cardService : CardService) { }
  id!: string;
  card$!: Observable<Card>;

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id') || '';
    this.card$ = this.cardService.getCard(this.id).pipe(tap(console.log));//Forma mas adecuada sin subcribe, desde el html, para no crear una variable para guardar datos

    // this.cardService.getCard(this.id).subscribe(res => {
    //   console.log(res);
    // });
  }

}
